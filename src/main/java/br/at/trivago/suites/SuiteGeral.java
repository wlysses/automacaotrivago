package br.at.trivago.suites;
import static br.at.trivago.core.DriverFactory.killDriver;

import java.io.IOException;

import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.at.trivago.tests.DesafioCesar;
import br.at.trivago.core.Propriedades;


@RunWith(Suite.class)
@SuiteClasses({
	DesafioCesar.class,
})

public class SuiteGeral {
	@After
	public void finaliza() throws IOException{	
		if(Propriedades.FECHAR_BROWSER) {
			killDriver();
		}
	}
}

