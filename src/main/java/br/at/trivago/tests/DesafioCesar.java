package br.at.trivago.tests;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import br.at.trivago.pages.HomePage;
import br.at.trivago.core.BasePage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DesafioCesar {
	
	HomePage homePage = new HomePage();
	BasePage basePage = new BasePage();
	String pesquisa = "Natal";
	String qtdHospedes = "1";
	String qtdQuartos = "1";
	String ordenacao = "Somente dist�ncia";
	int indiceHotel = 2;
	
	//ABRE P�GINA INICIAL E REALIZA A PESQUISA
	@Test
	public void T1_realizarConsulta() throws InterruptedException {
		homePage.acessarPaginaInicial();
		
		//FAZ PESQUISA PASSANDO: LOCAL, HOSPEDES, QUARTOS E ORDENA��O
		homePage.realizarPesquisa(pesquisa, qtdHospedes,qtdQuartos,ordenacao);	
		
		Assert.assertEquals("Hot�is em "+ pesquisa + " (" + ordenacao + ") | Pesquise e compare �timas ofertas no trivago", homePage.obterTitlePage());
	}
	
	
	//IMPRIME INFORMA��ES DO HOTEL/QUARTO
	@Test
	public void T2_imprimeInformacoesHotel() throws InterruptedException {		
		System.out.println("Informa��es do hotel desejado");
		System.out.println("Nome: " + homePage.retornarNomeHotel(indiceHotel));
		System.out.println("Estrelas: " + homePage.retornaQtdEstrelas(indiceHotel,"content"));
		System.out.println("Oferta da empresa: " + homePage.retornaNomeEmpresa(indiceHotel));
		System.out.println("Pre�o: " + homePage.retornaValorQuarto(indiceHotel));			
	}
	
	
	
	
	
}
