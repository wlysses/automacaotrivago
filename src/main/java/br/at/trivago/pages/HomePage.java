package br.at.trivago.pages;

import static br.at.trivago.core.DriverFactory.getDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.at.trivago.core.*;

public class HomePage extends BasePage {
	
	WebDriverWait wait = new WebDriverWait(getDriver(), 10);
	
	public void acessarPaginaInicial(){
		DriverFactory.getDriver().get("http://www.trivago.com.br");
	}	
	public void realizarPesquisa(String valorPesquisa, String Qtdhospedes, String qtdQuartos, String valorOrdenar ) throws InterruptedException
	{
		//ESCREVE NO CAMPO DE PESQUISA
		escrever("querytext", valorPesquisa);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='ssg-suggestions']")));
		clicarBotao(By.xpath("//ul[@id='ssg-suggestions']/li[1]"));
		
		//CLICA NO BOT�O DE ESCOLHA DE QUARTOS
		clicarBotao(By.xpath("//*[@id=\"js-fullscreen-hero\"]/div[1]/form/div/button[1]"));
		
		//INFORMA A QUANTIDADE DE QUARTOS E H�SPEDES E CONFIRMA
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Confirmar']")));		
		escrever("adults-input", Qtdhospedes);
		escrever("rooms-input", qtdQuartos);
		clicarBotao(By.xpath("//button[text()='Confirmar']"));
		
		//CLICA NO BOT�O PESQUISAR
		clicarBotao(By.xpath("//*[@id=\"js-fullscreen-hero\"]/div[1]/form/div/button[2]"));	
		
		//ORDENA POR DISTANCIA		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("mf-select-sortby")));
		selecionarCombo("mf-select-sortby",valorOrdenar);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ol/li[1]")));
	
	}
	
	public String retornarNomeHotel(int indiceBusca)
	{
		//AGURADA A EXIBI��O DOS HOT�IS DISPON�VEIS
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]/div/article/div[1]/div[2]/div/div/h3/span")));
		return obterTexto(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]/div/article/div[1]/div[2]/div/div/h3/span"));
	}
	
	public String retornaQtdEstrelas(int indiceBusca, String atributo) throws InterruptedException
	{
		//AGURADA A EXIBI��O DOS HOT�IS DISPON�VEIS
		try {
			WebElement divStar = getDriver().findElement(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]/div/article/div[1]/div[2]/div/div/div[1]/div"));
			WebElement meta = divStar.findElement(By.xpath(".//meta"));			
			return meta.getAttribute("content");
			
		} catch (NoSuchElementException e) {
			return "0";
		}
	
	}
	
	public String retornaNomeEmpresa(int indiceBusca)
	{
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]//span[@data-qa='recommended-price-partner']")));
		return obterTexto(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]//span[@data-qa='recommended-price-partner']"));
		
	}
	
	public String retornaValorQuarto(int indiceBusca)
	{
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]//strong[@data-qa='recommended-price']")));
		return obterTexto(By.xpath("//ol[@id='js_itemlist']/li["+indiceBusca+"]//strong[@data-qa='recommended-price']"));
	}
	
	
	public String obterTitlePage()
	{
		return getDriver().getTitle();
	}

}
